/*
** server.h for server in /home/camill_n/rendu/PSU_2014_myirc/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu Apr  2 22:22:39 2015
** Last update Tue Apr  7 22:43:45 2015 
*/

#ifndef SERVER_H_
# define SERVER_H_

# define PORT 3333

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <stdint.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <strings.h>
#include <limits.h>

typedef struct sockaddr_in sock_in;

typedef struct		s_sock
{
  int			fd;
  sock_in		info;
}			t_sock;

typedef struct		s_client
{
  char			nick_name[9];
  char			host_name[HOST_NAME_MAX];
  t_sock		sock;
  struct s_client	*next;
}			t_client;

typedef struct		s_data
{
  t_client		*cli;
  t_sock		host;
  unsigned int		nb_cli;
  char			run;
}			t_data;


/* core/utils.c */

int			check_socket(int);
t_client		*add_cli(t_data *);
void			accept_cli(t_data *data);
void			del_cli(t_data *, t_client *);
void			init_srv_socket(t_sock *, int);

/* core/init.c */

void			init_data(t_data *);
void			init_host(t_data *);

/* core/run.c */



void			listen_loop(t_data *data);
void			receive_paquet(t_data *data, t_client **sock);
void			listen_loop(t_data *data);
int			run(t_data *);

#endif
