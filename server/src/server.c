//
// Created by camill_n on 02/04/15.
//

#include "../include/server.h"

int		main(int ac, char **av)
{
  t_data	data;

  init_data(&data);
  init_host(&data);
  run(&data);
  return (EXIT_SUCCESS);
}
