/*
** init.c for myirc in /home/camill_n/rendu/PSU_2014_myirc/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Apr  7 22:17:05 2015
** Last update Tue Apr  7 22:19:08 2015 
*/

#include "server.h"

void	init_data(t_data *data)
{
  data->cli = NULL;
  data->nb_cli = 0;
  data->run = 1;
}

void	init_host(t_data *data)
{
  init_srv_socket(&(data->host), PORT);
}
