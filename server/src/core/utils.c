/*
** utils.c for myirc in /home/camill_n/rendu/PSU_2014_myirc/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Apr  7 21:43:27 2015
** Last update Tue Apr  7 22:45:44 2015 
*/

#include "server.h"

int		check_socket(int sockfd)
{
  if (listen(sockfd, 1) < 0)
    {
      perror("listen");
      exit (EXIT_FAILURE);
    }
  return (0);
}

t_client	*add_cli(t_data *data)
{
    t_client	*tmp;
    t_client	*new_cli;

    if ((new_cli = malloc(sizeof(t_client))) == NULL)
      exit(0);
    new_cli->next = NULL;
    if (data->cli == NULL)
      data->cli = new_cli;
    else
      {
        tmp = data->cli;
        while (tmp)
	  tmp = tmp->next;
        tmp->next = new_cli;
      }
    ++(data->nb_cli);
    return (new_cli);
}

void		init_srv_socket(t_sock *host, int port)
{
  int		opt;

  printf("-> Initialisation du socket principal: ");
  opt = 1;
  bzero(&host->info, sizeof(host->info));
  if ((host->fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
      perror("socket");
      exit(-1);
    }
  host->info.sin_family = AF_INET;
  host->info.sin_port = htons(port);
  host->info.sin_addr.s_addr = INADDR_ANY;
  setsockopt(host->fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
  if (bind(host->fd, (struct sockaddr *)&host->info,
	   sizeof(host->info)) == -1)
    {
      perror("bind");
      exit(-1);
    }
  check_socket(host->fd);
  printf("\033[32mDone.\033[0m\n");
}

void		del_cli(t_data *data, t_client *cli)
{
    t_client	*tmp;

    if (!data->nb_cli)
      return ;
    if (data->cli != NULL && data->cli->next == NULL)
      data->cli = NULL;
    else if (data->cli == cli)
      data->cli = cli->next;
    else
      {
	tmp = data->cli;
	while (tmp->next != cli)
	  tmp = tmp->next;
	if (cli->next != NULL)
	  tmp->next = cli->next;
	else
	  tmp->next = NULL;
      }
    --(data->nb_cli);
    printf("A client get out (still %d client)\n", data->nb_cli);
    close(cli->sock.fd);
    free(cli);
}

void		accept_cli(t_data *data)
{
  t_client	*cli;
  int		size;

  printf("-> New client detected !\n");
  cli = add_cli(data);
  bzero(&(cli->sock.info), sizeof(cli->sock.info));
  size = 0;
  cli->sock.fd = accept(data->host.fd,
			 (struct sockaddr *)&(cli->sock.info),
			 (socklen_t *)&size);
  if (cli->sock.fd == -1)
    {
      perror("socket");
      exit(-1);
    }
}
