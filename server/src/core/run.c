/*
** run.c for myirc in /home/camill_n/rendu/PSU_2014_myirc/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Apr  7 22:14:13 2015
** Last update Tue Apr  7 22:55:37 2015 
*/

#include "server.h"

void		check_sock(t_data *data, fd_set *rdfs)
{
  t_client	*cli;

  if (FD_ISSET(data->host.fd, rdfs))
    accept_cli(data);
  cli = data->cli;
  while (cli)
    {
      if (FD_ISSET(cli->sock.fd, rdfs))
	receive_paquet(data, &cli);
      if (cli)
	cli = cli->next;
    }
}

void		get_higher_sock(t_data *data, int *hcli)
{
  t_client	*cli;

  *hcli = data->host.fd;
  cli = data->cli;
  while (cli)
    if (cli->sock.fd > *hcli)
      (*hcli = cli->sock.fd);
    else
      (cli = cli->next);
}

void		receive_paquet(t_data *data, t_client **cli)
{
  char		buff[128];
  int		ret;
  t_client	*tmp;

  ret = read((*cli)->sock.fd, buff, 128);
  if (!ret)
    {
      tmp = (*cli)->next;
      del_cli(data, *cli);
      *cli = tmp;
    }
  else
    {
      buff[ret] = 0;
      printf("CLIENT SEND: %s\n", buff);
    }
}

int	run(t_data *data)
{
  t_client	*cli;
  fd_set	rdfs;
  int		highest_sock;

  printf("Daemon start\n");
  highest_sock = data->host.fd;
  while (data->run)
    {
      get_higher_sock(data, &highest_sock);
      FD_ZERO(&rdfs);
      cli = data->cli;
      FD_SET(data->host.fd, &rdfs);
      while (cli)
	{
	  FD_SET(cli->sock.fd, &rdfs);
	  cli = cli->next;
	}
      if (select(highest_sock + 1, &rdfs, NULL, NULL, NULL) < 0)
	{
	  perror("select");
	  exit(EXIT_FAILURE);
	}
      check_sock(data, &rdfs);
      usleep(100);
    }
  return (0);
}
