##
## Makefile for malloc in /home/camill_n/rendu/PSU_2014_malloc
##
## Made by Nicolas Camilli
## Login   <camill_n@epitech.net>
##
## Started on  Mon Feb  9 18:32:06 2015 Nicolas Camilli
## Last update Wed Apr  8 10:05:05 2015 
##

CLI_DIR = client/
SRV_DIR = server/

NAME 	= my_irc

all:	 $(NAME)

$(NAME):
	 make -C $(CLI_DIR)
	 make -C $(SRV_DIR)

clean:
	make clean -C $(CLI_DIR)
	make clean -C $(SRV_DIR)

fclean:
	make fclean -C $(CLI_DIR)
	make fclean -C $(SRV_DIR)

re: fclean all

