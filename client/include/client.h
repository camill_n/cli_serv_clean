/*
** server.h for server in /home/camill_n/rendu/PSU_2014_myirc/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu Apr  2 22:22:39 2015
** Last update Wed Apr  8 09:54:22 2015 
*/

#ifndef CLIENT_H_
# define CLIENT_H_

# define HOST "127.0.0.1"
# define PORT 3333

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <netdb.h>
#include <stdint.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

typedef struct		s_host
{
  int			fd;
  long			addr_host;
  struct hostent	*host_info;
  struct sockaddr_in	info;
}			t_host;

typedef struct		s_data
{
  t_host		host;
  char			run;
}			t_data;

/* src/core/utils.c */

int	connect_server(t_data *, char *);

/* src/core/run.c */

int	run(t_data *);

#endif
