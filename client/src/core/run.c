/*
** run.c for myirc in /home/camill_n/rendu/PSU_2014_myirc/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Apr  7 22:14:13 2015
** Last update Wed Apr  8 09:51:45 2015 
*/

#include "client.h"

void		receive_paquet(t_data *data)
{
  char		buff[128];
  int		ret;
  ret = read(data->host.fd, buff, 128);
  buff[ret] = 0;
  printf("SRV SEND: %s\n", buff);
}

void		send_paquet(t_data *data)
{
  char		buff[128];
  int		ret;

  ret = read(0, buff, 128);
  buff[ret] = 0;
  write(data->host.fd, buff, strlen(buff));
  printf("SEND TO SRV: %s\n", buff);
}

void		check_sock(t_data *data, fd_set *rdfs)
{
  if (FD_ISSET(data->host.fd, rdfs))
    receive_paquet(data);
  if (FD_ISSET(0, rdfs))
    send_paquet(data);
}

int	run(t_data *data)
{
  fd_set	rdfs;

  printf("Daemon start\n");
  while (data->run)
    {
      FD_ZERO(&rdfs);
      FD_SET(data->host.fd, &rdfs);
      FD_SET(0, &rdfs);
      if (select(data->host.fd + 1, &rdfs, NULL, NULL, NULL) < 0)
	{
	  perror("select");
	  exit(EXIT_FAILURE);
	}
      check_sock(data, &rdfs);
      usleep(100);
    }
  return (0);
}
