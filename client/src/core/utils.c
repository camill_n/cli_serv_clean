/*
** sock.c for scarity_server in /home/camill_n/scarity/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun May 11 15:22:22 2014 camill_n
** Last update Wed Apr  8 09:55:04 2015 
*/

#include "client.h"

int	connect_server(t_data *data, char *srv)
{
  bzero(&(data->host.info), sizeof(data->host.info));
  data->host.addr_host = inet_addr(srv);
  if ((long)data->host.addr_host != (long)-1)
    bcopy(&(data->host.addr_host),
	  &(data->host.info.sin_addr), sizeof(data->host.addr_host));
  else
    {
      data->host.host_info = gethostbyname(srv);
      if (!data->host.host_info)
	{
	  printf("Server is not reachable\n");
	  return (0);
	}
      bcopy(data->host.host_info->h_addr,
	    &(data->host.info.sin_addr), data->host.host_info->h_length);
    }
  data->host.info.sin_port = htons(PORT);
  data->host.info.sin_family = AF_INET;
  if ((data->host.fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    return (0 * printf("Fail during the creation of socket\n"));
  if (connect(data->host.fd, (struct sockaddr *)&(data->host.info),
	      sizeof(data->host.info)) < 0)
    exit(printf("Connexion refused..\n") * 0 + EXIT_FAILURE);
  return (1);
}
