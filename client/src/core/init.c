/*
** init.c for myirc in /home/camill_n/rendu/PSU_2014_myirc/server
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Apr  7 22:17:05 2015
** Last update Wed Apr  8 09:59:05 2015 
*/

#include "client.h"

void	init_data(t_data *data)
{
  data->run = 1;
}

void	init_host(t_data *data)
{
  connect_server(data, HOST);
}
